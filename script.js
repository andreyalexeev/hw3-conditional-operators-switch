"use srict";

// task 1

let userInput = prompt("Введіть свій вік:");

if (isNaN(userInput)) {
  alert("Введено не число. Будь ласка, введіть число.");
} else {
  
  let age = userInput;

  if (age < 12) {
    alert("Ви є дитиною.");
  } else if (age < 18) {
    alert("Ви є підлітком.");
  } else {
    alert("Ви є дорослим.");
  }
}

// task 2

// let month = prompt("Введіть місяць року (маленкими літерами):");

// switch (month) {
//   case "січень":
//   case "березень":
//   case "травень":
//   case "липень":
//   case "серпень":
//   case "жовтень":
//   case "грудень":
//     console.log("У цьому місяці 31 день.");
//     break;

//   case "квітень":
//   case "червень":
//   case "вересень":
//   case "листопад":
//     console.log("У цьому місяці 30 днів.");
//     break;

//   case "лютий":
//     console.log("У цьому місяці 28 або 29 днів (залежно від року).");
//     break;

//   default:
//     console.log("Невірно введений місяць.");
// }

